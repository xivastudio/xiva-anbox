#!/bin/bash


# Criar arquivo de log
exec 2> >(tee -a /var/log/install-anbox-xiva.log)
exec 1>&2


### Verificação de pré-instalação ###

# Nome do usuário
who=$(who | awk '{ print $1 }' | head -n 1)

# Verificar versão do kernel
kernelversion=$(uname -r | cut -d "." -f 1)
kernelversionfull=$(uname -r | cut -d "-" -f 1)
# Verificar tipo de kernel
kerneltype=$(uname -r | cut -d "-" -f 2)

# Se o kernel for 5.x ou maior e/ou kernel for liquorix
if [ "$kernelversion" -ge "5" -o "$kerneltype" = liquorix ]; then
    zenity --width=400 --info --title="Erro Encontrado" --text="A sua versão do kernel não é compativel\nConsidere mudar de kernel\nVersão: $kernelversionfull\nTipo: $kerneltype"
    # Sair
    exit 1
fi


### Instalação do Anbox ###

# Instalar o Anbox
sudo snap install --devmode --beta anbox

# Verificar se o Anbox devmode tá instalado
veranbox=$(snap list | egrep "anbox|devmode")
if [ -z "$veranbox" ]; then
    sudo snap install --devmode --beta anbox
    # Verificar novamente
    if [ -z "$veranbox" ]; then
        zenity --width=400 --info --title="Erro Encontrado" --text="Algo errado não está certo\nVeja o log em /var/log/install-anbox-xiva.log\nEntre em contato com o suporte"
        # Não instalado
        exit 1
    fi
fi

# Instalar o adb
sudo apt install -y android-tools-adb

# Verificar se o adb foi instalado
veradb=$(dpkg -l | grep android-tools-adb)
if [ -z "$veradb" ]; then
    sudo apt install -y android-tools-adb
    # Verificar novamente
    if [ -z "$veradb" ]; then
        zenity --width=400 --info --title="Erro Encontrado" --text="Algo errado não está certo\nVeja o log em /var/log/install-anbox-xiva.log\nEntre em contato com o suporte"
        # Não instalado
        exit 1
    fi
fi
    
# Verificar se o anbox-modules-dkms está disponível para instalar
anboxmodule=$(apt search anbox-modules-dkms | grep anbox-modules-dkms)
# Adiciona o repositório de módulos de kernel
if [ -z "$anboxmodule" ]; then
    # Adiciona o repositório de módulos de kernel
    sudo add-apt-repository -y ppa:morphis/anbox-support
    #instalar modulos de kernel
    sudo apt install -y anbox-modules-dkms
    # Se não estiver disponível
    if [ -z "$anboxmodule" ]; then 
        zenity --width=400 --info --title="Erro Encontrado" --text="Algo errado não está certo\nVeja o log em /var/log/install-anbox-xiva.log\nEntre em contato com o suporte"
        exit 1
    fi
fi

# Carregar os módulos de kernel
sudo modprobe ashmem_linux
sudo modprobe binder_linux

# Verificar os módulos de kernel
if [ ! -e "/dev/binder" -a ! -e "/dev/ashmem" ]; then
    sudo apt install -y --reinstall anbox-modules-dkms
	sudo modprobe ashmem_linux
	sudo modprobe binder_linux
	# Verificar novamente
    if [ ! -e "/dev/binder" -a ! -e "/dev/ashmem" ]; then 
        zenity --width=400 --info --title="Erro Encontrado" --text="Algo errado não está certo\nVeja o log em /var/log/install-anbox-xiva.log\nEntre em contato com o suporte"
        #Não instalado
        exit 1
    fi
fi

# Se o ambiente gráfico for KDE, não desliga os efeitos do compositor
if [ "$XDG_CURRENT_DESKTOP" = KDE ]; then
    Compositing=$(cat $HOME/.config/kwinrc | grep WindowsBlockCompositing=true)
    if [ -z $Compositing ]; then
        kwriteconfig5 --file $HOME/.config/kwinrc --group Compositing --key WindowsBlockCompositing "false"
        qdbus org.kde.KWin /KWin reconfigure
        kquitapp5 plasmashell && kstart5 plasmashell
    fi
fi


### Montar pasta do /home no Anbox ao fazer login no sistema (sem necessidade de senha) ###

# Criar arquivo de sudoers permitindo a montagem sem pedir senha
sudo echo "$who ALL = NOPASSWD:/usr/bin/anbox-mount" | sudo tee /etc/sudoers.d/anbox-mount
# Copiar o arquivo de montagem para /usr/bin
sudo cp anbox-mount /usr/bin
# Dar permissão de execução ao arquivo
chmod +x /usr/bin/anbox-mount
# Adicionar a montagem ao boot do user
cp anbox-mount.desktop /home/$who/.config/autostart


### Suporte ARM ###

set -e

# OPENGAPPS_RELEASEDATE="20180903"
# OPENGAPPS_FILE="open_gapps-x86_64-7.1-mini-$OPENGAPPS_RELEASEDATE.zip"
# OPENGAPPS_URL="https://github.com/opengapps/x86_64/releases/download/$OPENGAPPS_RELEASEDATE/$OPENGAPPS_FILE"

HOUDINI_URL="http://dl.android-x86.org/houdini/7_y/houdini.sfs"
HOUDINI_SO="https://github.com/Rprop/libhoudini/raw/master/4.0.8.45720/system/lib/libhoudini.so"

COMBINEDDIR="/var/snap/anbox/common/combined-rootfs"
OVERLAYDIR="/var/snap/anbox/common/rootfs-overlay"
WORKDIR="/tmp/anbox-work"

# check if script was started with BASH
if [ ! "$(ps -p $$ -oargs= | awk '{print $1}' | grep -E 'bash$')" ]; then
   echo "Please use BASH to start the script!"
	 exit 1
fi

# check if user is root
if [ "$(whoami)" != "root" ]; then
	echo "Sorry, you are not root. Please run with sudo $0"
	exit 1
fi

# check if lzip is installed
if [ ! "$(which lzip)" ]; then
	sudo apt install -y lzip
fi

# check if squashfs-tools are installed
if [ ! "$(which mksquashfs)" ] || [ ! "$(which unsquashfs)" ]; then
	sudo apt install -y squashfs-tools
else
	MKSQUASHFS=$(which mksquashfs)
	UNSQUASHFS=$(which unsquashfs)
fi

# check if wget is installed
if [ ! "$(which wget)" ]; then
	sudo apt install -y wget
else
	WGET=$(which wget)
fi

# check if unzip is installed
if [ ! "$(which unzip)" ]; then
	sudo apt install -y unzip
else
	UNZIP=$(which unzip)
fi

# check if tar is installed
if [ ! "$(which tar)" ]; then
	sudo apt install -y tar
else
	TAR=$(which tar)
fi

# use sudo if installed
if [ ! "$(which sudo)" ]; then
	SUDO=""
else
	SUDO=$(which sudo)
fi


if [ ! -d "$COMBINEDDIR" ]; then
  # enable overlay fs
  $SUDO snap set anbox rootfs-overlay.enable=true
  $SUDO snap restart anbox.container-manager

  sleep 20
fi

echo $OVERLAYDIR
if [ ! -d "$OVERLAYDIR" ]; then
    echo -e "Overlay no enabled ! Please check error messages!"
	exit 1
fi

echo $WORKDIR
if [ ! -d "$WORKDIR" ]; then
    mkdir "$WORKDIR"
fi

gitdir=$(echo $PWD)

cd "$WORKDIR"

if [ -d "$WORKDIR/squashfs-root" ]; then
  $SUDO rm -rf squashfs-root
fi

# get image from anbox
cp /snap/anbox/current/android.img .
$SUDO $UNSQUASHFS android.img

# get opengapps and install it
# cd "$WORKDIR"
# if [ ! -f ./$OPENGAPPS_FILE ]; then
#   $WGET -q --show-progress $OPENGAPPS_URL
#   $UNZIP -d opengapps ./$OPENGAPPS_FILE
# fi


# cd ./opengapps/Core/
# for filename in *.tar.lz
# do
#     $TAR --lzip -xvf ./$filename
# done

# cd "$WORKDIR"
# APPDIR="$OVERLAYDIR/system/priv-app" 
# if [ ! -d "$APPDIR" ]; then
# 	$SUDO mkdir -p "$APPDIR"
# fi

# $SUDO cp -r ./$(find opengapps -type d -name "PrebuiltGmsCore")					$APPDIR
# $SUDO cp -r ./$(find opengapps -type d -name "GoogleLoginService")				$APPDIR
# $SUDO cp -r ./$(find opengapps -type d -name "Phonesky")						$APPDIR
# $SUDO cp -r ./$(find opengapps -type d -name "GoogleServicesFramework")			$APPDIR

# cd "$APPDIR"
# $SUDO chown -R 100000:100000 Phonesky GoogleLoginService GoogleServicesFramework PrebuiltGmsCore

# load houdini and spread it
cd "$WORKDIR"
if [ ! -f ./houdini.sfs ]; then
  #$WGET -q --show-progress $HOUDINI_URL
  cp $gitdir/arm/houdini.sfs .
  mkdir -p houdini
  $SUDO $UNSQUASHFS -f -d ./houdini ./houdini.sfs
fi

BINDIR="$OVERLAYDIR/system/bin"
if [ ! -d "$BINDIR" ]; then
   $SUDO mkdir -p "$BINDIR"
fi

$SUDO cp -r ./houdini/houdini "$BINDIR/houdini"

$SUDO cp -r ./houdini/xstdata "$BINDIR/"
$SUDO chown -R 100000:100000 "$BINDIR/houdini" "$BINDIR/xstdata"

LIBDIR="$OVERLAYDIR/system/lib"
if [ ! -d "$LIBDIR" ]; then
   $SUDO mkdir -p "$LIBDIR"
fi

#$SUDO $WGET -q --show-progress -P "$LIBDIR" $HOUDINI_SO
cp $gitdir/arm/libhoudini.so "$LIBDIR"
$SUDO chown -R 100000:100000 "$LIBDIR/libhoudini.so"

$SUDO mkdir -p "$LIBDIR/arm"
$SUDO cp -r ./houdini/linker "$LIBDIR/arm"
$SUDO cp -r ./houdini/*.so "$LIBDIR/arm"
$SUDO cp -r ./houdini/nb "$LIBDIR/arm"

$SUDO chown -R 100000:100000 "$LIBDIR/arm"

# add houdini parser
$SUDO mkdir -p "$OVERLAYDIR/system/etc/binfmt_misc"
echo ":arm_dyn:M::\x7f\x45\x4c\x46\x01\x01\x01\x00\x00\x00\x00\x00\x00\x00\x00\x00\x03\x00\x28::/system/bin/houdini:" >> "$OVERLAYDIR/system/etc/binfmt_misc/arm_dyn"
echo ":arm_exe:M::\x7f\x45\x4c\x46\x01\x01\x01\x00\x00\x00\x00\x00\x00\x00\x00\x00\x02\x00\x28::/system/bin/houdini:" >> "$OVERLAYDIR/system/etc/binfmt_misc/arm_exe"
$SUDO chown -R 100000:100000 "$OVERLAYDIR/system/etc/binfmt_misc"

# add features
C=$(cat <<-END
  <feature name="android.hardware.touchscreen" />\n
  <feature name="android.hardware.audio.output" />\n
  <feature name="android.hardware.camera" />\n
  <feature name="android.hardware.camera.any" />\n
  <feature name="android.hardware.location" />\n
  <feature name="android.hardware.location.gps" />\n
  <feature name="android.hardware.location.network" />\n
  <feature name="android.hardware.microphone" />\n
  <feature name="android.hardware.screen.portrait" />\n
  <feature name="android.hardware.screen.landscape" />\n
  <feature name="android.hardware.wifi" />\n
  <feature name="android.hardware.bluetooth" />"
END
)


C=$(echo $C | sed 's/\//\\\//g')
C=$(echo $C | sed 's/\"/\\\"/g')

if [ ! -d "$OVERLAYDIR/system/etc/permissions/" ]; then
  $SUDO mkdir -p "$OVERLAYDIR/system/etc/permissions/"
  $SUDO cp "$WORKDIR/squashfs-root/system/etc/permissions/anbox.xml" "$OVERLAYDIR/system/etc/permissions/anbox.xml"
fi

$SUDO sed -i "/<\/permissions>/ s/.*/${C}\n&/" "$OVERLAYDIR/system/etc/permissions/anbox.xml"

# make wifi and bt available
$SUDO sed -i "/<unavailable-feature name=\"android.hardware.wifi\" \/>/d" "$OVERLAYDIR/system/etc/permissions/anbox.xml"
$SUDO sed -i "/<unavailable-feature name=\"android.hardware.bluetooth\" \/>/d" "$OVERLAYDIR/system/etc/permissions/anbox.xml"

if [ ! -x "$OVERLAYDIR/system/build.prop" ]; then
  $SUDO cp "$WORKDIR/squashfs-root/system/build.prop" "$OVERLAYDIR/system/build.prop"
fi

# set processors
ARM_TYPE=",armeabi-v7a,armeabi"
$SUDO sed -i "/^ro.product.cpu.abilist=x86_64,x86/ s/$/${ARM_TYPE}/" "$OVERLAYDIR/system/build.prop"
$SUDO sed -i "/^ro.product.cpu.abilist32=x86/ s/$/${ARM_TYPE}/" "$OVERLAYDIR/system/build.prop"

$SUDO echo "persist.sys.nativebridge=1" >> "$OVERLAYDIR/system/build.prop"

# enable opengles
$SUDO echo "ro.opengles.version=131072" >> "$OVERLAYDIR/system/build.prop"

$SUDO snap restart anbox.container-manager

$SUDO rm -r $WORKDIR
