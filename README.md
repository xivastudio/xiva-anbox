# Anbox com suporte a ARM

Baixe o script *install-anbox-xiva.sh* e execute-o com o comando:
```bash
git clone git@gitlab.com:xivastudio/xiva-anbox.git
cd xiva-anbox
sudo bash -x install-anbox-xiva.sh
```

## Instalando APKs

Abra o Anbox:
```bash
anbox.appmgr
```
Entre na pasta onde se encontra o APK, e execute o comando:
```bash
cd pasta-com-apk
adb install nomedoarquivo.apk
```

## Para integrar o aplicativo ao lançador (iniciar)

Copie o arquivo .desktop do apk dentro de ~/snap/anbox/common/app-data/applications/anbox/ e cole dentro de  ~/.local/share/applications/

## Contribuindo
Qualquer contribuição é bem-vinda.
Crie um issue com suas ideias e discuta a possibilidade de implementá-las. Sempre faça os devidos testes antes de submeter as melhorias.

## Licença
GPL
