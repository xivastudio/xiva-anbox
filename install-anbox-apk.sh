#!/bin/bash

### script p/ ser rodado como user, não funciona em .deb ###


#nome do aplicativo deve ser o mesmo nome do .desktop gerado por ele
nome=
apk=/tmp/$nome.apk
anbox_desktop=anbox-com-android-$nome.desktop

#descobrir nome do user
who=$(who | awk '{ print $1 }' | head -n 1)

#verificar anbox devmode instalado
veranbox=$(snap list | egrep "anbox|devmode")
if [ -z "$veranbox" ]; then
    sudo snap install --devmode --beta anbox
    #verificar denovo se errado sair
    if [ -z "$veranbox" ]; then
        exit 1
    fi
fi

#verificar anbox-modules-dkms instalado
veranboxdkms=$(dpkg -l | grep anbox-modules-dkms)
if [ -z "$veranboxdkms" ]; then
    sudo apt install -y --reinstall anbox-modules-dkms
    #verificar denovo se errado sair
    veranboxdkms=$(dpkg -l | grep anbox-modules-dkms)
    if [ -z "$veranboxdkms" ]; then
        exit 1
    fi
fi

#verificar modulos de kernel
if [ ! -e "/dev/binder" -a ! -e "/dev/ashmem" ]; then
    sudo apt install -y --reinstall anbox-modules-dkms
	sudo modprobe ashmem_linux
	sudo modprobe binder_linux
	#verificar denovo se errado sair
    if [ ! -e "/dev/binder" -a ! -e "/dev/ashmem" ]; then 
        exit 1
    fi
fi

#logar como user
#su $who

#verificar se server anbox ta ok
veryadb=$(adb devices | tail -n 1)

#verificar ser server anbox ta ativo se não ativar
if [ -z "$veryadb" ]; then
    /snap/bin/anbox session-manager &
    #esperar 20 segundo
    sleep 20
fi

#instalar apk
if [ -n "$veryadb" ]; then
    adb install $apk
fi

#fechar server
#killall anbox

#copiar .desktop p/ pasta /usr/share/applications/
cp /home/$who/snap/anbox/common/app-data/applications/$anbox_desktop /usr/share/applications/$anbox_desktop

### desinstalar
#adb uninstall app.apk
